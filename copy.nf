#!/usr/bin/env nextflow
 

original_file = file('/bulk/bjsco/SRR057477.fastq') /* Copying original file */
original_file.copyTo('/homes/bilala/bin/BIOL_890/biol_890/Next_Flow/original_file.fastqc') 
/*
Getting the copied file into paramters

*/
params.master_file = file('/homes/bilala/bin/BIOL_890/biol_890/Next_Flow/original_file.fastqc')

/*
Creating a channel from parmeter file and making a new small test file
*/
master_file_ch = Channel.from(params.master_file)

process small_file {
        publishDir '/homes/bilala/bin/BIOL_890/biol_890/Next_Flow'
        input:
		file 'master' from master_file_ch
		output:
		file 'test_file*' into smaller_file_ch
	    """
		head -n 100 $master >> test_file.fastq
		
		"""
} 

/*
Finding and counting the total number of lines and lines starting with @
*/

process find {
       input:
	   file 'smaller_file' from smaller_file_ch
	   output:
	   file "result_file*" into result_ch
	   """
	   cat '$smaller_file' |wc -l >> result_file.txt
	   grep "^@" '$smaller_file'|wc -l >> result_file.txt
	   """



} 
/*
Code Review From Edward.
I have just a few recomendations.
One things that is not nesicarry in this case, but would be nesicarry when handeling many in put files is setting up some sort of sample name in file outputs. This can be done by setting sample when stating the input, and then can be used in output file names.
The second thing I would recommend is a tag statement for every process. This combined with the sample labels from above allows for greater debugging later on. This tags the log with a unique identifier for every iteration for easy tracking
Another thing you might consider adding is a completion status. This can be done using the workflow.onComplete command, and can let you know some statistics of your workflow.
As far as your actual code goes I do have a few comments/questions. My fiust is that in the process small_file you are always writing to the same output file, as you do not have any unique sample identfier, this would not work with mutiple samples.
You could also do a combination of head and tail like this
head -n 40 $master >> test_file.fastq
tail -n 40 $master >> test_file.fastq
Although I would recommend adding a unique smaple identifier so that you can have a sample file for each fastq that is input.
The reason you would want to take a head and tail is that sometime the beginning of fastq files can have lower quality data
Another sugesstion/question I have is in the find process
you seem to have two commands here, one where you are reading "smaller file" and then piping it into word count, and counting lines.
The other command is counting taking all the lines that contain a @ at the begging of the line and then piping it into word count lines.
For this I am confused at the purpose of the first command?
One other comment I have is that while this method of taking all the lines with @ at the begining works most of the time, there is one case where this dose not.
This is becase @ is also a quality score in fastq's this means with larger datasets it is likely that you may find a few quality score lines that also start with @
To get around this problem we could do somthing like '(cat input_file |wc -l)/4 | bc ' This does a few things for us. Firstly it reads the input with cat, and then pipes it to word count which counts lines.
Then we are left with somthing that looks like (number_of_lines)/4 which will then be piped into binary calculator, which will then divide our number of lines by 4 giving us the exact number of sequences.
*/